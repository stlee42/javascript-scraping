var puppeteer = require('puppeteer');

/*
 * A function is a block of code that can be run repeatedly. It has a name and
 * can be called like any other method. Defining a function does not cause it to
 * be called. Think of it like giving someone a sheet of paper with a set of
 * instructions on it.
 *
 * The "async" word here means that this function runs "asynchronous" code - ie,
 * code that takes a while to execute. The async part of this is important,
 * because it allows us to introduce timings which make it even harder for a
 * website to fingerprint us.
 */
async function run () {
    //Notice that instead of indentation, Javascript uses brackets.
    //An opening bracket means you're entering a block, a closing bracket means you're closing the block.

    /*
     * This is a little bit different from what we were doing in Python. In this
     * case, we're launching a literal browser, not just querying the page.
     *
     * So here, we boot up headless chrome, headless meaning that no UI will be
     * shown - the browser will run entirely in the background.
     *
     * "await" goes hand in hand with "async" above. It basically means 'wait
     * for this command to finish, even if it takes a while.'
     */
    var browser = await puppeteer.launch({
        args: ['--no-sandbox'],
        headless: true
    });

    //Open a new tab in the browser.
    var page = await browser.newPage();

    /*
     * Now, manta.com is tricksy. They want to block headless browsers.
     * So, we need to make sure that they can't.
     * Luckily, we have complete control over the browser, so we can fake all of the stuff they would check.
     * This part is more complicated, so feel free to just skip over it if you're not interested.
     *
     * Manta's detection turns out to actually be pretty basic, so... meh, not much to do here.
     */
    var userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.39 Safari/537.36';
    await page.setUserAgent(userAgent);
    await page.evaluateOnNewDocument(function () {
        Object.defineProperty(navigator, 'webdriver', { get: function () { return false; } });
    });
    async function pauseLikeAHuman (seconds) {
        return new Promise(function (res) {
            setTimeout(res, 1000 * seconds);
        });
    }

    /*
     * Okay, back to normal stuff. Start paying attention again.
     */
    var url = 'https://www.manta.com/search?search=contractors&pg=1&pt=39.2904,-76.6122&search_location=Baltimore%20MD';
    await page.goto(url, { //Navigate to manta.com
        waitUntil: 'networkidle2' //Wait for the browser to stop making requests (otherwise the page might not be fully loaded)
    });

    /*
     * One final test that manta uses to detect bots is to artificially delay
     * certain elements from loading. To get around this, we're going to
     * occasionally pause for a second or two, like a human would.
     *
     * We fully *expect* to be rate limited, and we're honestly pretty OK with
     * being rate limited. This script won't need to be run all that often, so
     * if we have to add 2 or 3 minutes, or even 5 or 6 minutes, or heck even
     * 10-12 minutes to the runtime because we wait around and act human-like...
     * that's perfectly fine.
     */
    await pauseLikeAHuman(1); //pause for one second.
    console.log(await page.$('.organic-result a.pull-left'));

    //screenshot the page
    await page.screenshot({
        path : 'headless-test-result.png'
    });

    //close the browser
    await browser.close();
}

run();
